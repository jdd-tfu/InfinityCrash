package com.infinityCrash.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.infinityCrash.ICHelpers.Constants;
import com.infinityCrash.infinityCrash.ICGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width=Constants.DESKTOP_WIDTH;
		config.height=Constants.DESKTOP_HEIGHT;
		new LwjglApplication(new ICGame(), config);
	}
}
