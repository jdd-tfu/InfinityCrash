package com.infinityCrash.ICHelpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import GameWorld.GameWorld;

/**
 * Created by iam47888006 on 5/21/15.
 */
public class ServerConnection {

    public static void saveScore(final GameWorld gameWorld){
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://dreamlo.com/lb/hvB_N97ZiE66eBmwdrfbFASMuUFo2mXUmizfw9-_59yw/add/" + gameWorld.getPlayer().getUsername() + "/" + gameWorld.getScore());
                    InputStream is = url.openStream();
                    is.close();
                } catch (Exception e) {
                    return;
                }
            }
        });
    }

    public static void loadScore(String[] userScore){
        URL url;
        HttpURLConnection connection = null;
        InputStream is = null;
        JsonValue parser = null;
        try {
            url = new URL("http://dreamlo.com/lb/55560a9b6e51b60ed886038d/json/3");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            is = connection.getInputStream();
            parser = new JsonReader().parse(is);
            userScore[0] = parser.child().child().child().get(0).getString(0) + ": " + parser.child().child().child().get(0).getString(1);
            userScore[1] = parser.child().child().child().get(1).getString(0) + ": " + parser.child().child().child().get(1).getString(1);
            userScore[2] = parser.child().child().child().get(2).getString(0) + ": " + parser.child().child().child().get(2).getString(1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
