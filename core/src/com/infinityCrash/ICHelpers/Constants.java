package com.infinityCrash.ICHelpers;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Constants {

	public static final String GAME_NAME = "InfinityCrash";
	// Tamany pantalla escriptori
	public static final int DESKTOP_WIDTH = 1280;
	public static final int DESKTOP_HEIGHT = 720;
	// Tamany pantalla dispositiu
	public static final float SCREEN_WIDTH = Gdx.graphics.getWidth();
	public static final float SCREEN_HEIGHT = Gdx.graphics.getHeight();
	// Tamany JOC
	public static final int GAME_WIDTH = 10;
	public static final int GAME_HEIGHT = 5;
	//MIDPOINT Y
	public static final int MID_POINT_Y = (int) (GAME_HEIGHT / 2);
	//RESIZE/SCALE_SPRITE
	public static final float DESIRED_WIDTH = SCREEN_WIDTH * .7f;
	
	//WORLD
	public static final Vector2 WORLD_GRAVITY = new Vector2(0, 10);
	
	//SIZE OBJECTS
	//BOX
	public static final float BOX_1_X = 13;
	public static final float BOX_1_Y = 3.5f;
	public static final float BOX_1_WIDTH = 0.5f;
	public static final float BOX_1_HEIGHT = 0.5f;
	//GROUND
	public static final float GROUND_OBJECT_X = -4;
	public static final float GROUND_OBJECT_Y = MID_POINT_Y + 2f;
	public static final int GROUND_OBJECT_WIDTH = GAME_WIDTH + 4;
	public static final int GROUND_OBJECT_HEIGHT = 0;
	//RECTANGLE GROUND
	public static final float GROUND_RECTANGLE_Y = MID_POINT_Y + 1.5f;

	//GRASS
	public static final float GRASS_1_X = 0;

	//PLAYER
	public static final float PLAYER_X = 2;
	public static final float PLAYER_Y = MID_POINT_Y + 0.5f;
	public static final float PLAYER_WIDTH = 0.5f;
	public static final float PLAYER_HEIGHT = 0.5f;

	// SCROLLHANDLER



	//FISICAS faltan fuerzas y impulsos etc.
	public static final float BOX_DENSITY = 1f;
	public static final float GROUND_DENSITY = 1f;
	public static final float PLAYER_DENSITY = 1f;


	// IMG/ATLAS/PACKS
	public static final String BACKGROUND = "data/menuBackground.jpg";
	public static final String BACKGROUND_CITY = "data/NNvsriO.png";
	public static final String LOGO_IMAGE_PATH = "data/logo.png";
	public static final String RUNNER_ATLAS_PATH = "data/crocodile/runPack.atlas";
	public static final String BOX_IMAGE_PATH = "data/sprite_box8bit1.png";
	public static final String BOX_EXPLOSION_ATLAS_PATH = "data/8bitBoxPack.atlas";

	// SOUNDS


	// FONT
	public static final String GREEN_FONT_PATH = "data/font.fnt";

	// HOLE
	public static final float HOLE_WIDTH = 0.8f;
	public static final float HOLE_HEIGHT = 0;

}