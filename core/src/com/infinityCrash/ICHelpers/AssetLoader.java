package com.infinityCrash.ICHelpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.infinityCrash.Screens.MainMenu;

import static com.infinityCrash.ICHelpers.Constants.*;

public class AssetLoader {

	public static Texture logoTexture, box, backgroundMenu, gameBackground, musicON, musicOFF, ranking_online, ranking_offline;
	public static Texture zero, one, two, three, four, five, six, seven, eight, nine;
	public static Texture nebula,ready, gameOver, retry,gameOverRetryTexture,gameOverBackTexture, infinityGamesTitle, rank1, rank2, rank3, effectsON, effectsOFF, imagenCocodrilo, imagenTigre, imagenCocodriloGris, imagenTigreGris;
	public static Texture buttonDown, platform, boxReward, star1, suelo;
	public static TextureRegion logo, musicON_TR, musicOFF_TR, jumpUp, jumpDown, gameOverBack, gameOverRetry, play, pause;
	public static Animation playerAnimation, dizzyAnimation, slideAnimation;
	public static Animation boxExplosion, holeSkewers, boxRewardExplosion;
	public static Music mainMusic;
	public static Sound jump;
	public static BitmapFont font;
	public static Preferences prefs;

	public static Texture[] scores;

	public static void load() {
		suelo = new Texture(Gdx.files.internal("data/suelo.png"));
		ranking_online = new Texture(Gdx.files.internal("data/button_ranking_online.png"));
		ranking_offline = new Texture(Gdx.files.internal("data/button_ranking_offline.png"));

		gameOverBackTexture = new Texture(Gdx.files.internal("data/button_back.png"));
		gameOverRetryTexture = new Texture(Gdx.files.internal("data/button_retry.png"));
		musicON = new Texture(Gdx.files.internal("data/music_on.png"));
		musicOFF = new Texture(Gdx.files.internal("data/music_off.png"));
		effectsON = new Texture(Gdx.files.internal("data/button_effects_on.png"));
		effectsOFF = new Texture(Gdx.files.internal("data/button_effects_off.png"));
		
		imagenCocodrilo = new Texture(Gdx.files.internal("data/imagenCocodrilo.png"));
		imagenCocodriloGris = new Texture(Gdx.files.internal("data/imagenCocodriloGris.png"));
		imagenTigre = new Texture(Gdx.files.internal("data/imagenTigre.png"));
		imagenTigreGris = new Texture(Gdx.files.internal("data/imagenTigreGris.png"));

        musicON_TR = new TextureRegion(musicON,musicON.getWidth(),musicON.getHeight());
        musicOFF_TR = new TextureRegion(musicOFF,musicOFF.getWidth(),musicOFF.getHeight());

		backgroundMenu = new Texture(Gdx.files.internal(BACKGROUND));
		gameBackground = new Texture(Gdx.files.internal(BACKGROUND_CITY));

		logoTexture = new Texture(Gdx.files.internal(LOGO_IMAGE_PATH));
		logoTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		logo = new TextureRegion(logoTexture, 0, 0, 512, 114);
		infinityGamesTitle = new Texture(Gdx.files.internal("data/infinitygames.png"));

		platform = new Texture(Gdx.files.internal("data/platform.png"));

		rank1 = new Texture(Gdx.files.internal("data/copa_oro.png"));
		rank2 = new Texture(Gdx.files.internal("data/copa_plata.png"));
		rank3 = new Texture(Gdx.files.internal("data/copa_bronze.png"));
		ready = new Texture(Gdx.files.internal("data/ready.png"));
		retry = new Texture(Gdx.files.internal("data/retry.png"));
		gameOver = new Texture(Gdx.files.internal("data/gameover.png"));
		gameOverBack = new TextureRegion(gameOverBackTexture,gameOverBackTexture.getWidth(),gameOverBackTexture.getHeight());
		gameOverRetry = new TextureRegion(gameOverRetryTexture,gameOverRetryTexture.getWidth(),gameOverRetryTexture.getHeight());

		TextureAtlas boxExplosionAtlas = new TextureAtlas(Gdx.files.internal(BOX_EXPLOSION_ATLAS_PATH));
		for (TextureRegion tR : boxExplosionAtlas.getRegions()) {
			tR.flip(false, true);
		}
		boxExplosion = new Animation(1 / 30f, boxExplosionAtlas.getRegions());

		TextureAtlas boxRewardExplosionAtlas = new TextureAtlas(Gdx.files.internal("data/8bitBoxRewardPack.atlas"));
		for (TextureRegion tR : boxRewardExplosionAtlas.getRegions()) {
			tR.flip(false, true);
		}
		boxRewardExplosion = new Animation(1 / 30f, boxRewardExplosionAtlas.getRegions());

		box = new Texture(Gdx.files.internal(BOX_IMAGE_PATH));
		boxReward = new Texture(Gdx.files.internal("data/boxReward8bit.png"));

		mainMusic = Gdx.audio.newMusic(Gdx.files.internal("data/mainmusic.wav"));
		jump = Gdx.audio.newSound(Gdx.files.internal("data/jumping.wav"));

		nebula = new Texture(Gdx.files.internal("data/nebula.png"));
		star1 = new Texture(Gdx.files.internal("data/sunBackground.png"));
		Texture playTexture = new Texture(Gdx.files.internal("data/play.png"));
		play = new TextureRegion(playTexture, playTexture.getWidth(), playTexture.getHeight());
		Texture pauseTexture = new Texture(Gdx.files.internal("data/pause.png"));
		pause = new TextureRegion(pauseTexture, pauseTexture.getWidth(), pauseTexture.getHeight());

		font = new BitmapFont(Gdx.files.internal(GREEN_FONT_PATH));
		font.setScale(0.3f, -0.3f);

		buttonDown = new Texture(Gdx.files.internal("data/arrow-down.png"));

		zero = new Texture(Gdx.files.internal("data/zero.png"));
		one = new Texture(Gdx.files.internal("data/1.png"));
		two = new Texture(Gdx.files.internal("data/2.png"));
		three = new Texture(Gdx.files.internal("data/3.png"));
		four = new Texture(Gdx.files.internal("data/4.png"));
		five = new Texture(Gdx.files.internal("data/5.png"));
		six = new Texture(Gdx.files.internal("data/6.png"));
		seven = new Texture(Gdx.files.internal("data/7.png"));
		eight = new Texture(Gdx.files.internal("data/8.png"));
		nine = new Texture(Gdx.files.internal("data/9.png"));

//		holeSkewers = new Texture(Gdx.files.internal(FIRE_IMG_PATH));
		TextureAtlas skewersAtlas = new TextureAtlas(Gdx.files.internal("data/skewers/skewersPack.atlas"));
		for (TextureRegion tR : skewersAtlas.getRegions()) {
			tR.flip(false, true);
		}
		holeSkewers = new Animation(1 / 4f, skewersAtlas.getRegions());

		scores = new Texture[]{zero, one, two, three, four, five, six, seven, eight, nine};

		// Create (or retrieve existing) preferences file
		prefs = Gdx.app.getPreferences("InfinityCrashPrefs");

		if (!prefs.contains("highScore")) {
			prefs.putInteger("highScore", 0);
		}
		if (!prefs.contains("secondScore")) {
			prefs.putInteger("secondScore", 0);
		}
		if (!prefs.contains("thirdScore")) {
			prefs.putInteger("thirdScore", 0);
		}
		if (!prefs.contains("music")) {
			prefs.putBoolean("music", true);
		}
		if (!prefs.contains("effects")) {
			prefs.putBoolean("effects", true);
		}
	}

	public static void LoadPlayer() {
		if (MainMenu.crocodile) {

			TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.internal(RUNNER_ATLAS_PATH));
			for (TextureRegion tR : textureAtlas.getRegions()) {
				tR.flip(false, true);
			}
			textureAtlas.getRegions().reverse();

			playerAnimation = new Animation(1 / 10f, textureAtlas.getRegions());

			TextureAtlas textureAtlasJumper = new TextureAtlas(Gdx.files.internal("data/crocodile/jumpUpPack.atlas"));
			for (TextureRegion tR : textureAtlasJumper.getRegions()) {
				tR.flip(false, true);
			}
			jumpUp = textureAtlasJumper.findRegion("frame");

			TextureAtlas textureAtlasJumperFall = new TextureAtlas(Gdx.files.internal("data/crocodile/jumpFallPack.atlas"));
			for (TextureRegion tR : textureAtlasJumperFall.getRegions()) {
				tR.flip(false, true);
			}
			jumpDown = textureAtlasJumperFall.findRegion("frame");

			TextureAtlas textureAtlasDizzy = new TextureAtlas(Gdx.files.internal("data/crocodile/dizzyPack.atlas"));
			for (TextureRegion tR : textureAtlasDizzy.getRegions()) {
				tR.flip(false, true);
			}

			dizzyAnimation = new Animation(1 / 5f, textureAtlasDizzy.getRegions());

			TextureAtlas textureAtlasSlide = new TextureAtlas(Gdx.files.internal("data/crocodile/slidingPack.atlas"));
			for (TextureRegion tR : textureAtlasSlide.getRegions()) {
				tR.flip(false, true);
			}

			slideAnimation = new Animation(1 / 5f, textureAtlasSlide.getRegions());

		} else {
			TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.internal("data/tiger/runDemonPack.atlas"));
			for (TextureRegion tR : textureAtlas.getRegions()) {
				tR.flip(false, true);
			}
			textureAtlas.getRegions().reverse();

			playerAnimation = new Animation(1 / 10f, textureAtlas.getRegions());

			TextureAtlas textureAtlasJumper = new TextureAtlas(Gdx.files.internal("data/tiger/jumpDemonPack.atlas"));
			for (TextureRegion tR : textureAtlasJumper.getRegions()) {
				tR.flip(false, true);
			}
			jumpUp = textureAtlasJumper.findRegion("jump-up");
			jumpDown = textureAtlasJumper.findRegion("jump-fall");

			TextureAtlas textureAtlasDizzy = new TextureAtlas(Gdx.files.internal("data/tiger/dizzyDemonPack.atlas"));
			for (TextureRegion tR : textureAtlasDizzy.getRegions()) {
				tR.flip(false, true);
			}

			dizzyAnimation = new Animation(1 / 5f, textureAtlasDizzy.getRegions());

			TextureAtlas textureAtlasSlide = new TextureAtlas(Gdx.files.internal("data/tiger/slidingDemonPack.atlas"));
			for (TextureRegion tR : textureAtlasSlide.getRegions()) {
				tR.flip(false, true);
			}

			slideAnimation = new Animation(1 / 5f, textureAtlasSlide.getRegions());
		}
	}

	public static void setHighScore(int val) {
		prefs.putInteger("highScore", val);
		prefs.flush();
	}

	public static void setSecondScore(int val) {
		prefs.putInteger("secondScore", val);
		prefs.flush();
	}

	public static void setThirdScore(int val) {
		prefs.putInteger("thirdScore", val);
		prefs.flush();
	}

	public static int getSecondScore() {
		return prefs.getInteger("secondScore");
	}

	public static int getThirdScore() {
		return prefs.getInteger("thirdScore");
	}

	public static int getHighScore() {
		return prefs.getInteger("highScore");
	}

	public static void dispose() {
		// We must dispose of the texture when we are finished.

		// Dispose sounds

		jump.dispose();
		mainMusic.dispose();
		font.dispose();
	}

}