package com.infinityCrash.ICHelpers;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.infinityCrash.GameObjects.Player;
import com.infinityCrash.Screens.MainMenu;
import com.infinityCrash.infinityCrash.ICGame;
import com.infinityCrash.ui.SimpleButton;
import static com.infinityCrash.ICHelpers.Constants.*;


import GameWorld.GameWorld;

public class InputHandler implements InputProcessor {
	private Player myPlayer;
	private GameWorld myWorld;

	private SimpleButton gameOverBackButton, gameOverRetryButton;
	private static SimpleButton playPauseButton;

    private float scaleFactorX;
	private float scaleFactorY;

	private ICGame game;

	public InputHandler(ICGame game, GameWorld myWorld, float scaleFactorX,
			float scaleFactorY) {
		this.game = game;
		this.myWorld = myWorld;
		myPlayer = myWorld.getPlayer();

		this.scaleFactorX = scaleFactorX;
		this.scaleFactorY = scaleFactorY;

		gameOverBackButton = new SimpleButton(GAME_WIDTH / 2 + 1 ,
				MID_POINT_Y + 1, 1, 1, AssetLoader.gameOverBack,
				AssetLoader.gameOverBack);

		gameOverRetryButton = new SimpleButton(GAME_WIDTH / 2 - 2,
				MID_POINT_Y + 1, 1, 1, AssetLoader.gameOverRetry,
				AssetLoader.gameOverRetry);

		playPauseButton = new SimpleButton(GAME_WIDTH - 1,
				0, 1, 1, AssetLoader.pause,
				AssetLoader.play);
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		screenX = scaleX(screenX);
		screenY = scaleY(screenY);

		if (myWorld.isReady()) {
			myWorld.start();
			myPlayer.onClick(false);
		} else if (myWorld.isRunning()) {
			if (screenX >= 9 && screenY >= 4) {
				myPlayer.onClickRight(true);
			} else if (screenX >= 8 && screenY >= 4) {
				myPlayer.onClick(true);
			} else if (playPauseButton.isTouchDown(screenX, screenY)) {
				myWorld.setGameState(GameWorld.GameState.PAUSED);
			} else {
				myPlayer.onClick(false);
			}
		} else if (myWorld.isPaused()) {
			if (playPauseButton.isTouchDown(screenX, screenY)) {
				myWorld.setGameState(GameWorld.GameState.RUNNING);
			}
		}

		if (myWorld.isGameOver()) {
			if(gameOverBackButton.isTouchDown(screenX, screenY)){
				game.setScreen(new MainMenu(game));
			}
			else if (gameOverRetryButton.isTouchDown(screenX, screenY)) {
				myWorld.restart();
			}
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		if (myWorld.isRunning()) {
			myPlayer.onClickRight(false);
		}

		return false;
	}

	@Override
	public boolean keyDown(int keycode) {

		// Can now use Space Bar to play the game
		if (keycode == Keys.SPACE) {
			if (myWorld.isReady()) {
				myWorld.start();
			}
			myPlayer.onClick(false);
			if (myWorld.isGameOver()) {
				myWorld.restart();
			}
		}

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	private int scaleX(int screenX) {
		return (int) (screenX / scaleFactorX);
	}

	private int scaleY(int screenY) {
		return (int) (screenY / scaleFactorY);
	}

	public static SimpleButton getPlayPauseButton() {
		return playPauseButton;
	}
}
