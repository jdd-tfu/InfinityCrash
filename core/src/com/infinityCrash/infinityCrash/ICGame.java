package com.infinityCrash.infinityCrash;

	import com.badlogic.gdx.Game;
import com.infinityCrash.ICHelpers.AssetLoader;
import com.infinityCrash.Screens.SplashScreen;

public class ICGame extends Game {

	@Override
	public void create() {
		AssetLoader.load();
		setScreen(new SplashScreen(this));
	}

	@Override
	public void dispose() {
		super.dispose();
		AssetLoader.dispose();
	}

}