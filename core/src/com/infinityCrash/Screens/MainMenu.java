package com.infinityCrash.Screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.infinityCrash.ICHelpers.AssetLoader;
import com.infinityCrash.ICHelpers.ServerConnection;
import com.infinityCrash.infinityCrash.ICGame;



public class MainMenu implements Screen {

    final ICGame game;
    private SpriteBatch batch;
    private BitmapFont font;
    private Stage stage;
    private Table table,tableMiddle;
    private Button start;
    private Button okButton;
    private Button settings;
    private Button ranking;
    private Button exit;
    private Button sw_music;
    private Button sw_effects;
    private Button sw_ranking;
    private Button changeUser;
    private Button back;
    private Viewport viewport;
    private OrthographicCamera cam;
    private Texture infinitycrashTitle;
    private Texture settingsTitle;
    private Texture rankingTitle;
    private Image image,img;
    private Image image2;
    private Image image3;
    private Image top1;
    private Image top2;
    private Image top3;
    private Table tableLeft;
    private Table tableRight;
    private Image imagenCocodrilo, imagenCocodriloGris, imagenTigre, imagenTigreGris, imgTutorial;
    private Label top, second, third;
    private String[] userScore;
    private Label.LabelStyle labelStyle;
    private com.badlogic.gdx.scenes.scene2d.ui.TextField textField;
    private TextButton.TextButtonStyle tbs_back,tbs_start,tbs_settings, tbs_ranking, tbs_exit,tbs_musicswitch, tbs_effects,
            tbs_rankingSW,tbs_changeUser;
    private Texture upImage_start,downImage_start,upImage_settings,downImage_settings,upImage_ranking,downImage_ranking,
            downImage_exit,upImage_back,upImage_exit, downImage_back, onImage_switch, offImage_switch, onImage_effects,
            onImage_rankingSW, offImage_effects, offImage_rankingSW, upImage_changeUser, downImage_changeUser;

    public static boolean crocodile = true;

    public MainMenu(final ICGame gam) {
        createMainMenu();
        createSettings();
        createRanking();
        exit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.exit(0);
            }
        });
        //STAGEADDS
        stage.addActor(img);
        stage.addActor(table);
        game = gam;
        if (!AssetLoader.prefs.contains("username")) {
            createDialog();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        batch.end();
    }

    @Override
    public void show() {
        //The elements are displayed in the order you add them.
        //The first appear on top, the last at the bottom.
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void createDialog(){
        //BUTTON DIALOG
        final Texture upImage_ok = new
                Texture(Gdx.files.internal("data/button_ok.png"));
        final Texture downImage_ok = new
                Texture(Gdx.files.internal("data/button_ok_pressed.png"));
        final TextButton.TextButtonStyle tbs_ok = new
                TextButton.TextButtonStyle();
        tbs_ok.font = font;
        tbs_ok.up = new TextureRegionDrawable(new
                TextureRegion(upImage_ok));
        tbs_ok.down = new TextureRegionDrawable(new TextureRegion(downImage_ok));
        okButton = new TextButton("", tbs_ok);

        //DIALOG

        com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle tfs = new com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle();
        tfs.font = font;
        tfs.background = new TextureRegionDrawable(new TextureRegion(new Texture("data/buttonDown.png")));
        tfs.fontColor = font.getColor();
        tfs.cursor = new TextureRegionDrawable(new TextureRegion(new Texture("data/cursor.png")));
        textField = new com.badlogic.gdx.scenes.scene2d.ui.TextField("", tfs);
        textField.setColor(Color.WHITE);
        Label message = new Label("Set Username",labelStyle);
        Window.WindowStyle ws2 = new Window.WindowStyle();
        ws2.titleFont = font;
        ws2.titleFontColor = Color.BLACK;
        Dialog dialog = new Dialog("", ws2);
        Texture dialog_background = new
                Texture(Gdx.files.internal("data/background-dialog.png"));
        //para que el resto de la pantalla/ventanas no sean clicables
        dialog.setModal(true);
        dialog.setBackground(new TextureRegionDrawable(new
                TextureRegion(dialog_background)));
        dialog.getContentTable().row().colspan(1).center();
        dialog.getContentTable().add(message).width(200f);
        dialog.getContentTable().row().expandX().colspan(2).minWidth(800);
        dialog.getContentTable().add(textField).width(300f);
        dialog.row().colspan(2); // 1 row, 2 columns
        dialog.button(okButton).padBottom(20f);
        dialog.setBounds(1920 / 2 -250, 1080 / 2 - 75, 500, 300);

        stage.addActor(dialog);
        okButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (textField.getText().equals("")){
                    AssetLoader.prefs.putString("username","unknown");
                }
                else{
                    AssetLoader.prefs.putString("username", textField.getText());
                }
                AssetLoader.prefs.flush();
            }
        });

    }

    public void createSettings(){
        settings.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                table.remove();
                //BUTTON CHANGEUSER
                upImage_changeUser = new
                        Texture(Gdx.files.internal("data/button_chname.png"));
                downImage_changeUser = new
                        Texture(Gdx.files.internal("data/button_chname_pressed.png"));
                tbs_changeUser = new
                        TextButton.TextButtonStyle();
                tbs_changeUser.up = new TextureRegionDrawable(new
                        TextureRegion(upImage_changeUser));
                tbs_changeUser.down = new TextureRegionDrawable(new TextureRegion(downImage_changeUser));
                tbs_changeUser.font = font;
                changeUser = new TextButton("", tbs_changeUser);

                table = new Table();
                settingsTitle = new Texture(Gdx.files.internal("data/settings.png"));
                image2 = new Image(settingsTitle);
                table.row();
                table.add(image2).padTop(50f).colspan(2).expand().padLeft(200f).padRight(200f);
                table.row();
                table.add().padTop(150f).colspan(2);
                table.row();
                table.add(changeUser).padTop(20f).colspan(2);
                changeUser.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        createDialog();
                    }
                });
                table.row();
                table.add(sw_effects).padTop(20f).colspan(2);
                sw_effects.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        if (AssetLoader.prefs.getBoolean("effects")) {
                            AssetLoader.prefs.putBoolean("effects", false);
                            AssetLoader.prefs.flush();
                        } else {
                            AssetLoader.prefs.putBoolean("effects", true);
                            AssetLoader.prefs.flush();
                        }
                    }
                });
                table.add().padTop(20f).colspan(2);
                table.row();
                table.add(sw_music).padTop(20f).colspan(2);
                sw_music.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        if (AssetLoader.prefs.getBoolean("music")) {
                            if (AssetLoader.mainMusic.isPlaying()) {
                                AssetLoader.mainMusic.stop();
                            }
                            AssetLoader.prefs.putBoolean("music", false);
                            AssetLoader.prefs.flush();
                        } else {
                            AssetLoader.prefs.putBoolean("music", true);
                            AssetLoader.prefs.flush();
                        }

                    }
                });
                table.add().padTop(20f).colspan(2);
                table.row();
                table.add(back).padTop(20f).colspan(2);
                table.padBottom(20f);
                table.setFillParent(true);
                table.pack();
                stage.addActor(table);
                back.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        game.setScreen(new MainMenu(game));
                        dispose();
                    }
                });
            }
        });

    }

    public void createTutorial() {
        img.remove();
        Image imgTutorial = new Image(new Texture(Gdx.files.internal("data/ingameTutorial.png")));
        imgTutorial.setPosition(0, 0);
        imgTutorial.setHeight(1080);
        imgTutorial.setWidth(1920);
        stage.addActor(imgTutorial);
        table.remove();
        back = new TextButton("", tbs_back);
        table = new Table();
        table.row();
        table.add(back).padBottom(-900f);
        table.setFillParent(true);
        stage.addActor(table);
        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MainMenu(game));
                dispose();
            }
        });
    }

    public void createRanking(){
        ranking.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                table.remove();
                back = new TextButton("", tbs_back);
                table = new Table();
                rankingTitle = new Texture(Gdx.files.internal("data/ranking.png"));
                image3 = new Image(rankingTitle);
                userScore = new String[3];
                ServerConnection.loadScore(userScore);
                top1 = new Image(AssetLoader.rank1);
                top2 = new Image(AssetLoader.rank2);
                top3 = new Image(AssetLoader.rank3);
                top = new Label(userScore[0], labelStyle);
                second = new Label(userScore[1], labelStyle);
                third = new Label(userScore[2], labelStyle);
                table.row();
                table.add(image3).padTop(50f).colspan(2).expand().padLeft(200f).padRight(200f);
                table.row();
                table.add(top1).padTop(150f).right().uniform();
                table.add(top).padTop(150f).left().uniform();
                table.row();
                table.add(top2).padTop(30f).right().uniform();
                table.add(second).padTop(30f).left().uniform();
                table.row();
                table.add(top3).padTop(30f).right().uniform();
                table.add(third).padTop(30f).left().uniform();
                table.row();
                table.add(sw_ranking).padTop(0f).colspan(2);
                sw_ranking.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        if (sw_ranking.isChecked()) {
                            top = new Label(String.valueOf(AssetLoader.getHighScore()), labelStyle);
                            second = new Label(String.valueOf(AssetLoader.getSecondScore()), labelStyle);
                            third = new Label(String.valueOf(AssetLoader.getThirdScore()), labelStyle);
                            table.remove();
                            table = new Table();
                            table.row();
                            table.add(image3).padTop(50f).colspan(2).expand().padLeft(200f).padRight(200f);
                            table.row();
                            table.add(top1).padTop(150f).right().uniform();
                            table.add(top).padTop(150f).left().uniform();
                            table.row();
                            table.add(top2).padTop(30f).right().uniform();
                            table.add(second).padTop(30f).left().uniform();
                            table.row();
                            table.add(top3).padTop(30f).right().uniform();
                            table.add(third).padTop(30f).left().uniform();
                            table.row();
                            table.add(sw_ranking).padTop(0f).colspan(2);
                            table.row();
                            table.add(back).padTop(50f).colspan(2);
                            table.padBottom(50f);
                            table.setFillParent(true);
                            table.pack();
                            stage.addActor(table);
                        } else {
                            top = new Label(userScore[0], labelStyle);
                            second = new Label(userScore[1], labelStyle);
                            third = new Label(userScore[2], labelStyle);
                            table.remove();
                            table = new Table();
                            table.row();
                            table.add(image3).padTop(50f).colspan(2).expand().padLeft(200f).padRight(200f);
                            table.row();
                            table.add(top1).padTop(150f).right().uniform();
                            table.add(top).padTop(150f).left().uniform();
                            table.row();
                            table.add(top2).padTop(30f).right().uniform();
                            table.add(second).padTop(30f).left().uniform();
                            table.row();
                            table.add(top3).padTop(30f).right().uniform();
                            table.add(third).padTop(30f).left().uniform();
                            table.row();
                            table.add(sw_ranking).padTop(0f).colspan(2);
                            table.row();
                            table.add(back).padTop(50f).colspan(2);
                            table.padBottom(50f);
                            table.setFillParent(true);
                            table.pack();
                            stage.addActor(table);
                        }
                    }
                });
                table.row();
                table.add(back).padTop(50f).colspan(2);
                table.padBottom(50f);
                table.setFillParent(true);
                table.pack();
                stage.addActor(table);
                back.addListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        game.setScreen(new MainMenu(game));
                        dispose();
                    }
                });
            }
        });
    }

    public void createMainMenu(){
        cam = new OrthographicCamera();
        cam.setToOrtho(false, 1920f, 1080f);
        batch = new SpriteBatch();
        stage = new Stage();
        viewport = new FitViewport(1920, 1080, cam);
        stage.setViewport(viewport);
        //Use LibGDX's default Arial font.
        font = new BitmapFont(Gdx.files.internal("data/font.fnt"));
        font.setScale(1.5f);
        labelStyle = new Label.LabelStyle(font,
                Color.WHITE);
        infinitycrashTitle = new Texture(Gdx.files.internal("data/infinitycrash.png"));
        image = new Image(infinitycrashTitle);
        //BUTTON START
        upImage_start = new
                Texture(Gdx.files.internal("data/button_start.png"));
        downImage_start = new
                Texture(Gdx.files.internal("data/button_start_pressed.png"));
        tbs_start = new
                TextButton.TextButtonStyle();
        tbs_start.font = font;
        tbs_start.up = new TextureRegionDrawable(new
                TextureRegion(upImage_start));
        tbs_start.down = new TextureRegionDrawable(new TextureRegion(downImage_start));
        start = new TextButton("", tbs_start);

        //BUTTON SETTINGS
        upImage_settings = new
                Texture(Gdx.files.internal("data/button_settings.png"));
        downImage_settings = new
                Texture(Gdx.files.internal("data/button_settings_pressed.png"));
        tbs_settings = new
                TextButton.TextButtonStyle();
        tbs_settings.up = new TextureRegionDrawable(new
                TextureRegion(upImage_settings));
        tbs_settings.down = new TextureRegionDrawable(new TextureRegion(downImage_settings));
        tbs_settings.font = font;
        settings = new TextButton("", tbs_settings);

        //BUTTON RANKING
        upImage_ranking = new
                Texture(Gdx.files.internal("data/button_ranking.png"));
        downImage_ranking = new
                Texture(Gdx.files.internal("data/button_ranking_pressed.png"));
        tbs_ranking = new
                TextButton.TextButtonStyle();
        tbs_ranking.up = new TextureRegionDrawable(new
                TextureRegion(upImage_ranking));
        tbs_ranking.down = new TextureRegionDrawable(new TextureRegion(downImage_ranking));
        tbs_ranking.font = font;
        ranking = new TextButton("", tbs_ranking);
        //BUTTON EXIT
        upImage_exit = new
                Texture(Gdx.files.internal("data/button_exit.png"));
        downImage_exit = new
                Texture(Gdx.files.internal("data/button_exit_pressed.png"));
        tbs_exit = new
                TextButton.TextButtonStyle();
        tbs_exit.up = new TextureRegionDrawable(new
                TextureRegion(upImage_exit));
        tbs_exit.down = new TextureRegionDrawable(new TextureRegion(downImage_exit));
        tbs_exit.font = font;
        exit = new TextButton("", tbs_exit);
        //BUTTON BACK
        upImage_back = new
                Texture(Gdx.files.internal("data/button_back.png"));
        downImage_back = new
                Texture(Gdx.files.internal("data/button_back_pressed.png"));
        tbs_back = new
                TextButton.TextButtonStyle();
        tbs_back.up = new TextureRegionDrawable(new
                TextureRegion(upImage_back));
        tbs_back.down = new TextureRegionDrawable(new TextureRegion(downImage_back));
        tbs_back.font = font;
        back = new TextButton("", tbs_back);

        //SOUND SWITCH
        onImage_switch = AssetLoader.musicON;
        offImage_switch = AssetLoader.musicOFF;
        tbs_musicswitch = new
                TextButton.TextButtonStyle();

        if (AssetLoader.prefs.getBoolean("music")) {
            tbs_musicswitch.up = new TextureRegionDrawable(new TextureRegion(onImage_switch));
            tbs_musicswitch.checked = new TextureRegionDrawable(new
                    TextureRegion(offImage_switch));
        } else {
            tbs_musicswitch.up = new TextureRegionDrawable(new TextureRegion(offImage_switch));
            tbs_musicswitch.checked = new TextureRegionDrawable(new
                    TextureRegion(onImage_switch));
        }
        tbs_musicswitch.font = font;
        sw_music = new TextButton("", tbs_musicswitch);

        //EFFECTS SWITCH
        onImage_effects = AssetLoader.effectsON;
        offImage_effects = AssetLoader.effectsOFF;
        tbs_effects = new
                TextButton.TextButtonStyle();

        if (AssetLoader.prefs.getBoolean("effects")) {
            tbs_effects.up = new TextureRegionDrawable(new TextureRegion(onImage_effects));
            tbs_effects.checked = new TextureRegionDrawable(new
                    TextureRegion(offImage_effects));
        } else {
            tbs_effects.up = new TextureRegionDrawable(new TextureRegion(offImage_effects));
            tbs_effects.checked = new TextureRegionDrawable(new
                    TextureRegion(onImage_effects));
        }
        tbs_effects.font = font;
        sw_effects = new TextButton("", tbs_effects);

        //RANKING ONLINE BUTTON
        onImage_rankingSW = AssetLoader.ranking_online;
        offImage_rankingSW = AssetLoader.ranking_offline;
        tbs_rankingSW = new
                TextButton.TextButtonStyle();
        tbs_rankingSW.up = new TextureRegionDrawable(new TextureRegion(onImage_rankingSW));
        tbs_rankingSW.checked = new TextureRegionDrawable(new TextureRegion(offImage_rankingSW));
        tbs_rankingSW.font = font;
        sw_ranking = new TextButton("", tbs_rankingSW);


        img = new Image(AssetLoader.backgroundMenu);
        imagenCocodrilo = new Image(AssetLoader.imagenCocodrilo);
        imagenCocodriloGris = new Image(AssetLoader.imagenCocodriloGris);
        imagenTigre = new Image(AssetLoader.imagenTigre);
        imagenTigreGris = new Image(AssetLoader.imagenTigreGris);
        imgTutorial = new Image(new Texture(Gdx.files.internal("data/question.png")));
        img.setPosition(0, 0);
        img.setHeight(1080);
        img.setWidth(1920);
        table = new Table();
        tableLeft = new Table();
        tableMiddle = new Table();
        tableRight = new Table();
        table.setPosition(0, 0);
        table.row();
        table.add(image).colspan(3).expand().padLeft(200f).padRight(200f);
        table.row();
        table.add(tableLeft).expand();
        table.add(tableMiddle).expand();
        table.add(tableRight).expand();
        table.row().colspan(3);
        table.add(imgTutorial).bottom().right();
        table.setFillParent(true);
        tableMiddle.row();
        tableMiddle.add(start).colspan(1).bottom().expand();
        tableMiddle.row();
        tableMiddle.add(settings).padTop(20f).colspan(1).expand();
        tableMiddle.row();
        tableMiddle.add(ranking).padTop(20f).colspan(1).expand();
        tableMiddle.row();
        tableMiddle.add(exit).padTop(20f).colspan(1).expand();
        tableMiddle.pack();
        tableLeft.row();
        tableLeft.right();
        tableLeft.add(imagenCocodrilo);
        tableRight.row();
        tableRight.left();
        tableRight.add(imagenTigreGris);
        tableRight.pack();
        tableLeft.pack();

        start.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                AssetLoader.LoadPlayer();
                game.setScreen(new GameScreen(game));
                dispose();
            }
        });
        imagenCocodriloGris.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                crocodile = true;
                tableLeft.removeActor(imagenCocodriloGris);
                tableLeft.add(imagenCocodrilo);
                tableRight.removeActor(imagenTigre);
                tableRight.add(imagenTigreGris);
            }
        });
        imagenTigreGris.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                crocodile = false;
                tableRight.removeActor(imagenTigreGris);
                tableRight.add(imagenTigre);
                tableRight.getCell(imagenTigre).getActorY();
                tableLeft.removeActor(imagenCocodrilo);
                tableLeft.add(imagenCocodriloGris);
            }
        });
        imgTutorial.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                createTutorial();
            }
        });
    }

}