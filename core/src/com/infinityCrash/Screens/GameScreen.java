package com.infinityCrash.Screens;

import GameWorld.GameRenderer;
import GameWorld.GameWorld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.infinityCrash.ICHelpers.InputHandler;
import com.infinityCrash.infinityCrash.ICGame;

import static com.infinityCrash.ICHelpers.Constants.*;

import static com.infinityCrash.ICHelpers.Constants.*;

public class GameScreen implements Screen {

	private GameWorld world;
	private GameRenderer renderer;
	private float runTime;

	// This is the constructor, not the class declaration
	public GameScreen(ICGame game) {

		world = new GameWorld();
		Gdx.input.setInputProcessor(new InputHandler(game, world, SCREEN_WIDTH / GAME_WIDTH, SCREEN_HEIGHT / GAME_HEIGHT));
		renderer = new GameRenderer(world, GAME_HEIGHT, MID_POINT_Y);
		world.setRenderer(renderer);
	}

	@Override
	public void render(float delta) {
		if (!world.isPaused()) {
			runTime += delta;
		}
		world.update(delta);
		renderer.render(delta, runTime);

	}

	@Override
	public void resize(int width, int height) {
		renderer.viewport.update(width, height);
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}