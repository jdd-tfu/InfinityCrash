package com.infinityCrash.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.infinityCrash.ICHelpers.AssetLoader;

public class SimpleButton {

	private float x, y, width, height;

	private TextureRegion buttonUp;
	private TextureRegion buttonDown;

	private Rectangle bounds;

	private boolean isPressed = false;
	private boolean isChecked = false;

	public SimpleButton(float x, float y, float width, float height,
			TextureRegion buttonUp, TextureRegion buttonDown) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.buttonUp = buttonUp;
		this.buttonDown = buttonDown;

		bounds = new Rectangle(x, y, width, height);

	}

	public boolean isClicked(float screenX, float screenY) {
		return bounds.contains(screenX, screenY);
	}

	public void draw(SpriteBatch batcher) {
		if (isChecked) {
			batcher.draw(buttonDown, x + 0.5f, 0.3f, 0.2f, 0.2f);
		} else {
			batcher.draw(buttonUp, x + 0.5f, 0.3f, 0.2f, 0.2f);
		}
	}

	public boolean isTouchDown(float screenX, float screenY) {

		if (bounds.contains(screenX, screenY) && screenX != bounds.getX() + bounds.getWidth() && screenY != bounds.getY() + bounds.getHeight()) {
			isPressed = true;
			if (!isChecked) {
				isChecked = true;
			} else {
				isChecked = false;
			}
			return true;
		}

		return false;
	}

	public boolean isTouchUp(float screenX, float screenY) {
		
		// It only counts as a touchUp if the button is in a pressed state.
		if (bounds.contains(screenX, screenY) && isPressed) {
			isPressed = false;
			AssetLoader.jump.play();
			return true;
		}
		
		// Whenever a finger is released, we will cancel any presses.
		isPressed = false;
		return false;
	}

	public boolean onOffMusic() {
		if (AssetLoader.mainMusic.isPlaying()) {
			AssetLoader.mainMusic.stop();
			return false;
		}
		AssetLoader.mainMusic.play();
		return true;
	}

	public void setIcon(TextureRegion texture) {
		this.buttonDown = texture;
		this.buttonUp = texture;
	}

}
