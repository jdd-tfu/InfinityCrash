package com.infinityCrash.GameObjects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.World;
import com.infinityCrash.ICHelpers.AssetLoader;

public class SimpleBox extends Box {

	public SimpleBox(World world, float x, float y, float width, float height, float scrollSpeed) {
		super(world, x, y, width, height, scrollSpeed);

		textureRegionBox = new TextureRegion(AssetLoader.box, 0, 0, AssetLoader.box.getWidth(), AssetLoader.box.getHeight());
		boxExplosion = AssetLoader.boxExplosion;
	}
}