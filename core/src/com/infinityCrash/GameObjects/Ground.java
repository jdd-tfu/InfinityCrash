package com.infinityCrash.GameObjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import static com.infinityCrash.ICHelpers.Constants.*;

public class Ground {
	
	private Vector2 position;
	private int width;
	private float height;
	
	Body body;
	
	public Ground(World world, float x, float y, int width, int height) {
		
		this.width = width;
		this.height = height;
		this.position = new Vector2(x, y);
		
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(new Vector2(this.getX(), this.getY()));
		Body body = world.createBody(bodyDef);
		this.body = body;
		PolygonShape polygonShape = new PolygonShape();
		polygonShape.setAsBox(this.getWidth()/2.0f, this.getHeight()/2.0f, new Vector2(this.getWidth()/2.0f, this.getHeight()/2.0f), 0);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = polygonShape;
		fixtureDef.density = GROUND_DENSITY;
		fixtureDef.filter.categoryBits = 1;
		fixtureDef.filter.maskBits = 1;
		body.createFixture(fixtureDef);
		polygonShape.dispose();
		
	}
	
	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}
	
	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public Body getBody() {
		return body;
	}

}
