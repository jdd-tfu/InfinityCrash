package com.infinityCrash.GameObjects;

public class Grass extends Scrollable {

	public Grass(float x, float y, int width, float f, float scrollSpeed) {
		super(x, y, width, f, scrollSpeed);
	}

	public void onRestart(float x, float scrollSpeed) {
		position.x = x;
		velocity.x = scrollSpeed;
	}

}
