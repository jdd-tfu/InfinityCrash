package com.infinityCrash.GameObjects;

import java.util.Random;

import GameWorld.GameWorld;

import com.badlogic.gdx.physics.box2d.World;
import com.infinityCrash.ICHelpers.AssetLoader;
import com.infinityCrash.ICHelpers.ServerConnection;

import static com.infinityCrash.ICHelpers.Constants.*;

public class ScrollHandler {

    private Grass frontGrass, backGrass;
    private SimpleBox simpleBox1, simpleBox2, simpleBox3, simpleBox4, simpleBox5, simpleBox6;
    private int randomGap, randomReward;
    private int SCROLL_SPEED;
    private Hole hole;
    private SimpleBox[] simpleBoxes;
    private Platform platform;
    private RewardBox rewardBox;


    private GameWorld gameWorld;

    public ScrollHandler(GameWorld gameWorld, World world, float groundHeight) {
        SCROLL_SPEED = -5;
        this.gameWorld = gameWorld;
        frontGrass = new Grass(GRASS_1_X, groundHeight + 0.5f, 15, 0.25f, SCROLL_SPEED);
        backGrass = new Grass(frontGrass.getTailX(), groundHeight + 0.5f, 15, 0.25f,
                SCROLL_SPEED);

        simpleBox1 = new SimpleBox(world, BOX_1_X, BOX_1_Y, BOX_1_WIDTH, BOX_1_HEIGHT, SCROLL_SPEED);
        simpleBox1.body.setUserData(simpleBox1);
        simpleBox2 = new SimpleBox(world, simpleBox1.getTailX() + (new Random()).nextInt(6) + 3, (new Random()).nextInt(1) + groundHeight - 1, 0.5f, 0.5f, SCROLL_SPEED);
        simpleBox2.body.setUserData(simpleBox2);
        simpleBox3 = new SimpleBox(world, simpleBox2.getTailX() + (new Random()).nextInt(6) + 3, (new Random()).nextInt(1) + groundHeight - 1, 0.5f, 0.5f, SCROLL_SPEED);
        simpleBox3.body.setUserData(simpleBox3);
        simpleBox4 = new SimpleBox(gameWorld.getWorld(), simpleBox3.getTailX() + (new Random()).nextInt(6) + 3, (new Random()).nextInt(1) + groundHeight - 1, 0.5f, 0.5f, SCROLL_SPEED);
        simpleBox4.body.setUserData(simpleBox4);
        simpleBox5 = new SimpleBox(world, simpleBox4.getTailX() + (new Random()).nextInt(6) + 3, (new Random()).nextInt(1) + groundHeight - 1, 0.5f, 0.5f, SCROLL_SPEED);
        simpleBox5.body.setUserData(simpleBox5);
        simpleBox6 = new SimpleBox(world, simpleBox5.getTailX() + (new Random()).nextInt(6) + 3, (new Random()).nextInt(1) + groundHeight - 1, 0.5f, 0.5f, SCROLL_SPEED);
        simpleBox6.body.setUserData(simpleBox6);

        simpleBoxes = new SimpleBox[]{simpleBox1, simpleBox2, simpleBox3, simpleBox4, simpleBox5, simpleBox6};

        hole = new Hole(world, SCROLL_SPEED);
        hole.body.setUserData(hole);

        platform = new Platform(world, -4, 4, 2, 0.2f, SCROLL_SPEED);
        platform.body.setUserData(platform);
        rewardBox = new RewardBox(world, platform.getX() + 1, platform.getY() - 0.5f, 0.5f, 0.5f, SCROLL_SPEED);
        rewardBox.body.setUserData(rewardBox);

    }

    public void updateReady(float delta) {

        frontGrass.update(delta);
        backGrass.update(delta);

        // Same with grass
        if (frontGrass.isScrolledLeft()) {
            frontGrass.reset(backGrass.getTailX());

        } else if (backGrass.isScrolledLeft()) {
            backGrass.reset(frontGrass.getTailX());
        }

    }

    public void update(float delta) {
        // Update our objects
        frontGrass.update(delta);
        backGrass.update(delta);
        simpleBox1.update(delta);
        simpleBox2.update(delta);
        simpleBox3.update(delta);
        simpleBox4.update(delta);
        simpleBox5.update(delta);
        simpleBox6.update(delta);
        hole.update(delta);
        platform.update(delta);
        rewardBox.update(delta);

        // Check if any of the simpleBoxes are scrolled left,
        // and reset accordingly
        randomGap = (int) (Math.random() * 3 + 3);
        randomReward = (int) (Math.random() * 100);

        if (rewardBox.destroyable) {
            gameWorld.getPlayer().resetLife();
            SCROLL_SPEED = -5;
            rewardBox.destroyable = false;
            rewardBox.onRestart(-5, platform.getY() - 0.5f, SCROLL_SPEED);
            gameWorld.addScore(1);
        }

        for (int i = 0; i < simpleBoxes.length; i++) {
            if (simpleBoxes[i].isScrolledLeft() || simpleBoxes[i].destroyable) {
                if (randomReward < 25) {
                    if (hole.getX() < -HOLE_WIDTH) {
                        hole.reset(GAME_WIDTH + 2);
                    }
                }
                if (randomReward <= 80 && randomReward > 60) {
                    if (platform.getX() < -platform.getWidth() - 1.5f) {
                        platform.reset(GAME_WIDTH + 1);
                        rewardBox.reset(platform.getTailX() + 1, platform.getY() - 0.5f);
                    }
                }
                if (simpleBoxes[i].isScrolledLeft()) {
                    gameWorld.getPlayer().setLife();
                } else {
                    if (randomReward <= 60 && randomReward > 40) {
                        gameWorld.getPlayer().addLife();
                    }
                    if (randomReward > 90)
                        SCROLL_SPEED = -6;
                    else if (randomReward < 10)
                        SCROLL_SPEED = -6;
                    else
                        SCROLL_SPEED = -5;
                    updateScrollSpeed(SCROLL_SPEED);
                }
                if (simpleBoxes[i].isScored)
                    gameWorld.addScore(1);
                simpleBoxes[i].destroyable = false;
                if (i == 0) {
                    simpleBoxes[i].reset(simpleBoxes[(simpleBoxes.length - 1)].getTailX() + randomGap);
                } else {
                    simpleBoxes[i].reset(simpleBoxes[i - 1].getTailX() + randomGap);
                }
            }
        }
        if (gameWorld.getPlayer().getLife() <= 0) {
            gameWorld.getPlayer().setDead();
            gameWorld.setGameState(GameWorld.GameState.GAMEOVER);
            //Thread para hacer la conexion con la bd sin ralentizar el juego.
            ServerConnection.saveScore(gameWorld);
            if (AssetLoader.getHighScore() < gameWorld.getScore()) {
                AssetLoader.setThirdScore(AssetLoader.getSecondScore());
                AssetLoader.setSecondScore(AssetLoader.getHighScore());
                AssetLoader.setHighScore(gameWorld.getScore());
            } else if (AssetLoader.getSecondScore() < gameWorld.getScore()) {
                AssetLoader.setThirdScore(AssetLoader.getSecondScore());
                AssetLoader.setSecondScore(gameWorld.getScore());
            } else if (AssetLoader.getThirdScore() < gameWorld.getScore()) {
                AssetLoader.setThirdScore(gameWorld.getScore());
            }
        }
        // Same with grass
        if (frontGrass.isScrolledLeft()) {
            frontGrass.reset(backGrass.getTailX());

        } else if (backGrass.isScrolledLeft()) {
            backGrass.reset(frontGrass.getTailX());
        }
    }

    public Grass getFrontGrass() {
        return frontGrass;
    }

    public Grass getBackGrass() {
        return backGrass;
    }

    public SimpleBox getSimpleBox1() {
        return simpleBox1;
    }

    public SimpleBox getSimpleBox2() {
        return simpleBox2;
    }

    public SimpleBox getSimpleBox3() {
        return simpleBox3;
    }

    public SimpleBox getSimpleBox4() {
        return simpleBox4;
    }

    public SimpleBox getSimpleBox5() {
        return simpleBox5;
    }

    public SimpleBox getSimpleBox6() {
        return simpleBox6;
    }

    public Hole getHole() {
        return hole;
    }

    public Platform getPlatform() {
        return platform;
    }

    public RewardBox getRewardBox() {
        return rewardBox;
    }

    public void onRestart() {
        SCROLL_SPEED = -5;
        frontGrass.onRestart(0, SCROLL_SPEED);
        backGrass.onRestart(frontGrass.getTailX(), SCROLL_SPEED);
        simpleBox1.onRestart(BOX_1_X, SCROLL_SPEED);
        simpleBox2.onRestart(simpleBox1.getTailX() + (new Random()).nextInt(6) + 3, SCROLL_SPEED);
        simpleBox3.onRestart(simpleBox2.getTailX() + (new Random()).nextInt(6) + 3, SCROLL_SPEED);
        simpleBox4.onRestart(simpleBox3.getTailX() + (new Random()).nextInt(6) + 3, SCROLL_SPEED);
        simpleBox5.onRestart(simpleBox4.getTailX() + (new Random()).nextInt(6) + 3, SCROLL_SPEED);
        simpleBox6.onRestart(simpleBox5.getTailX() + (new Random()).nextInt(6) + 3, SCROLL_SPEED);
        hole.onRestart(-HOLE_WIDTH - 0.4f, SCROLL_SPEED);
        platform.onRestart(-4, SCROLL_SPEED);
        rewardBox.onRestart(platform.getTailX() + 1, platform.getY() - 0.5f, SCROLL_SPEED);
    }

    public void updateScrollSpeed(int scrollSpeed) {
        frontGrass.setScrollSpeed(scrollSpeed);
        backGrass.setScrollSpeed(scrollSpeed);
        simpleBox1.setScrollSpeed(scrollSpeed);
        simpleBox2.setScrollSpeed(scrollSpeed);
        simpleBox3.setScrollSpeed(scrollSpeed);
        simpleBox4.setScrollSpeed(scrollSpeed);
        simpleBox5.setScrollSpeed(scrollSpeed);
        simpleBox6.setScrollSpeed(scrollSpeed);
        hole.setScrollSpeed(scrollSpeed);
        platform.setScrollSpeed(scrollSpeed);
        rewardBox.setScrollSpeed(scrollSpeed);
        if (scrollSpeed == -6)
            gameWorld.getPlayer().getPlayerAnimation().setFrameDuration(1 / 7f);
        else if (scrollSpeed == -5)
            gameWorld.getPlayer().getPlayerAnimation().setFrameDuration(1 / 10f);
        else if (scrollSpeed == -6)
            gameWorld.getPlayer().getPlayerAnimation().setFrameDuration(1 / 15f);
    }

}