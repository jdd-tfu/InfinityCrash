package com.infinityCrash.GameObjects;

import com.badlogic.gdx.math.Vector2;

public class Scrollable {
	
	
	// Protected is similar to private, but allows inheritance by subclasses.
	protected Vector2 position;
	protected Vector2 velocity;
	protected float width;
	protected float height;
	protected boolean isScrolledLeft;

	public Scrollable(float x, float y, float width2, float height2, float scrollSpeed) {
		position = new Vector2(x, y);
		velocity = new Vector2(scrollSpeed, 0);
		this.width = width2;
		this.height = height2;
		isScrolledLeft = false;
	}

	public void update(float delta) {
		position.add(velocity.cpy().scl(delta));

		// If the Scrollable object is no longer visible:

		if (this instanceof SimpleBox) {
			if (position.x + width < -1) {
				isScrolledLeft = true;
			}
		} else {
			if (position.x + width < 0) {
				isScrolledLeft = true;
			}
		}
	}

	// Reset: Should Override in subclass for more specific behavior.
	public void reset(float newX) {
		position.x = newX;
		isScrolledLeft = false;
	}
	
	// Getters for instance variables
	public boolean isScrolledLeft() {
		return isScrolledLeft;
	}

	public float getTailX() {
		return position.x + width;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public void setScrollSpeed(int scrollSpeed) {
		velocity = new Vector2(scrollSpeed, 0);
	}

}
