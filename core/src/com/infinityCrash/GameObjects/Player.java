package com.infinityCrash.GameObjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.infinityCrash.ICHelpers.AssetLoader;

import static com.infinityCrash.ICHelpers.Constants.*;

public class Player {

    private Vector2 position;

    private float width;
    private float height;

    private boolean isAlive;

    // Box2D
    private Body body;

    public boolean canJump;

    public boolean isLanded;

    private int life;

    public boolean destroyFixture;

    private Fixture fixturePlayer;

    private Animation playerAnimation;

    private boolean isDead;

    private String username;

    public Player(World world, float x, float y, float f, float g) {


        this.width = f;
        this.height = g;
        position = new Vector2(x, y);
        isAlive = true;
        life = 5;
        destroyFixture = false;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(this.getX(), this.getY()));
        bodyDef.fixedRotation = true;
        Body body = world.createBody(bodyDef);
        body.setGravityScale(3f);
        this.setBody(body);
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(this.getWidth() / 2.0f, this.getHeight() / 2.0f, new Vector2(this.getWidth() / 2.0f, this.getHeight() / 2.0f), 0);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = PLAYER_DENSITY;
        fixtureDef.filter.categoryBits = 1;
        fixtureDef.filter.maskBits = 1;
        fixturePlayer = body.createFixture(fixtureDef);
        polygonShape.dispose();

        if (AssetLoader.prefs.getBoolean("music")) {
            AssetLoader.mainMusic.play();
            AssetLoader.mainMusic.setLooping(true);
        }

        playerAnimation = AssetLoader.playerAnimation;

        isDead = false;
    }

    public void update(float delta) {
        position.set(body.getPosition());

    }

    public void onClick(boolean downOrUp) {
        if (downOrUp) {
            if (body.getLinearVelocity().y != 0) {
                body.applyLinearImpulse(0, 2.6f, body.getWorldCenter().x, body.getWorldCenter().y, true);
            }
        } else {
            if (isAlive && (canJump || isLanded)) {
                if (body.getLinearVelocity().y == 0) {
                    if (AssetLoader.prefs.getBoolean("effects")) {
                        AssetLoader.jump.play();
                    }
                    body.applyLinearImpulse(0, -2.6f, body.getWorldCenter().x, body.getWorldCenter().y, true);
                }
            }
        }
    }

    public void onClickRight(boolean touchUpDown) {
        if (touchUpDown) {
            this.getBody().destroyFixture(this.getBody().getFixtureList().first());
            PolygonShape shape;
            FixtureDef fdef;

            // Create box shape
            shape = new PolygonShape();
            shape.setAsBox(PLAYER_WIDTH / 4.0f, PLAYER_HEIGHT / 4.0f, new Vector2(PLAYER_WIDTH / 2.0f, PLAYER_HEIGHT / 2.0f), 0);


            // Create FixtureDef for player collision box
            fdef = new FixtureDef();
            fdef.shape = shape;
            fdef.density = PLAYER_DENSITY * 4;
            fdef.filter.categoryBits = 1;
            fdef.filter.maskBits = 1;

            // Create player collision box fixture
            this.getBody().createFixture(fdef).setUserData(this);
            shape.dispose();
            playerAnimation = AssetLoader.slideAnimation;
        } else {
            this.getBody().destroyFixture(this.getBody().getFixtureList().first());
            PolygonShape shape;
            FixtureDef fdef;

            // Create box shape
            shape = new PolygonShape();
            shape.setAsBox(PLAYER_WIDTH / 2.0f, PLAYER_HEIGHT / 2.0f, new Vector2(PLAYER_WIDTH / 2.0f, PLAYER_HEIGHT / 2.0f), 0);


            // Create FixtureDef for player collision box
            fdef = new FixtureDef();
            fdef.shape = shape;
            fdef.density = PLAYER_DENSITY;
            fdef.filter.categoryBits = 1;
            fdef.filter.maskBits = 1;

            // Create player collision box fixture
            this.getBody().createFixture(fdef).setUserData(this);
            shape.dispose();
            playerAnimation = AssetLoader.playerAnimation;
        }
    }

    public void onRestart() {
        this.resetLife();
        Filter f = new Filter();
        f.categoryBits = 1;
        f.maskBits = 1;
        this.getFixturePlayer().setFilterData(f);
        this.getBody().setTransform(PLAYER_X, PLAYER_Y, 0);
        isAlive = true;
        isDead = false;
        onClickRight(false);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Body getBody() {
        return this.body;
    }

    public void setLife() {
        this.life--;
    }

    public int getLife() {
        return this.life;
    }

    public void killPlayer() {
        this.life = 0;
    }

    public Fixture getFixturePlayer() {
        return this.fixturePlayer;
    }

    public void resetLife() {
        this.life = 5;
    }

    public void addLife() {
        if (this.getLife() < 5) this.life++;
    }

    public Animation getPlayerAnimation() {
        return this.playerAnimation;
    }

    public void setDead() {
        this.isDead = true;
    }

    public boolean getDead() {
        return this.isDead;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUserName(String username) { this.username = username; }

}
