package com.infinityCrash.GameObjects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.World;
import com.infinityCrash.ICHelpers.AssetLoader;

/**
 * Created by iam45790625 on 5/20/15.
 */
public class RewardBox extends Box {

    public RewardBox(World world, float x, float y, float width, float height, float scrollSpeed) {
        super(world, x, y, width, height, scrollSpeed);

        textureRegionBox = new TextureRegion(AssetLoader.boxReward, 0, 0, AssetLoader.box.getWidth(), AssetLoader.box.getHeight());
        boxExplosion = AssetLoader.boxRewardExplosion;
    }

    public void reset(float newX, float newY) {
        position.x = newX;
        isScrolledLeft = false;
        // Change the height to a random number
        position.y = newY;
        this.body.setTransform(this.getX(), this.getY(), 0);
        isScored = false;
    }

    public void onRestart(float x, float y, float scrollSpeed) {
        velocity.x = scrollSpeed;
        reset(x, y);
    }
}
