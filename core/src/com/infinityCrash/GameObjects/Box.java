package com.infinityCrash.GameObjects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import java.util.Random;

import static com.infinityCrash.ICHelpers.Constants.BOX_DENSITY;

/**
 * Created by iam45790625 on 5/21/15.
 */
public class Box extends Scrollable {

    private Random r;

    public Body body;

    public boolean destroyable;
    public boolean ISGONNABEDESTROYED;

    public TextureRegion textureRegionBox;

    public Animation boxExplosion;
    public float stateExplosion;

    public boolean isScored;

    private World world;

    public Box(World world, float x, float y, float width, float height, float scrollSpeed) {
        super(x, y, width, height, scrollSpeed);
        // Initialize a Random object for Random number generation
        r = new Random();

        this.world = world;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(new Vector2(this.getX(), this.getY()));
        Body body = world.createBody(bodyDef);
        this.body = body;
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(this.getWidth() / 2.0f, this.getHeight() / 2.0f, new Vector2(this.getWidth() / 2.0f, this.getHeight() / 2.0f), 0);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = BOX_DENSITY;
        fixtureDef.filter.categoryBits = 1;
        fixtureDef.filter.maskBits = 1;
        body.createFixture(fixtureDef);
        polygonShape.dispose();
        ISGONNABEDESTROYED = false;
        isScored = false;

        stateExplosion = 0;
    }

    @Override
    public void update(float delta) {
        // Call the update method in the superclass (Scrollable)
        super.update(delta);
    }

    @Override
    public void reset(float newX) {
        // Call the reset method in the superclass (Scrollable)
        super.reset(newX);
        // Change the height to a random number
        position.y = (float) r.nextDouble() + 2.5f;
        this.body.setTransform(this.getX(), this.getY(), 0);
        isScored = false;
    }

    public void onRestart(float x, float scrollSpeed) {
        velocity.x = scrollSpeed;
        reset(x);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
