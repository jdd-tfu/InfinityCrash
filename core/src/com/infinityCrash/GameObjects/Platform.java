package com.infinityCrash.GameObjects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.infinityCrash.ICHelpers.AssetLoader;

import java.util.Random;

/**
 * Created by iam45790625 on 5/19/15.
 */
public class Platform extends Scrollable {

    private World world;
    public Body body;
    private Random r;

    public TextureRegion textureRegionPlatform;

    public Platform(World world, float x, float y, float width, float height, float scrollSpeed) {
        super(x, y, width, height, scrollSpeed);
        this.world = world;
        // Initialize a Random object for Random number generation
        r = new Random();

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(new Vector2(this.getX(), this.getY()));
        Body body = world.createBody(bodyDef);
        this.body = body;
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(this.getWidth() / 2.0f, this.getHeight() / 2.0f, new Vector2(this.getWidth() / 2.0f, this.getHeight() / 2.0f), 0);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = 1;
        fixtureDef.filter.categoryBits = 1;
        fixtureDef.filter.maskBits = 1;
        body.createFixture(fixtureDef);
        polygonShape.dispose();

        textureRegionPlatform = new TextureRegion(AssetLoader.platform, 0, 0, AssetLoader.platform.getWidth(), AssetLoader.platform.getHeight());
        textureRegionPlatform.flip(false, true);
    }

    @Override
    public void update(float delta) {
        // Call the update method in the superclass (Scrollable)
        super.update(delta);
    }

    @Override
    public void reset(float newX) {
        // Call the reset method in the superclass (Scrollable)
        super.reset(newX);
        // Change the height to a random number
        position.y = (float) r.nextDouble() + 1.5f;
        this.body.setTransform(this.getX(), this.getY(), 0);
    }

    public void onRestart(float x, float scrollSpeed) {
        velocity.x = scrollSpeed;
        reset(x);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
