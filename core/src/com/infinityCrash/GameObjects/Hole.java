package com.infinityCrash.GameObjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import static com.infinityCrash.ICHelpers.Constants.*;

/**
 * Created by iam45790625 on 5/11/15.
 */
public class Hole extends Scrollable {

    public Body body;

    public Hole(World world, float scrollSpeed) {
        super(-HOLE_WIDTH - 0.4f, GROUND_OBJECT_Y, HOLE_WIDTH, HOLE_HEIGHT, scrollSpeed);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(new Vector2(this.getX(), this.getY()));
        Body body = world.createBody(bodyDef);
        this.body = body;
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(this.getWidth() / 2.0f, this.getHeight() / 2.0f, new Vector2(this.getWidth() / 2.0f, this.getHeight() / 2.0f), 0);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.filter.categoryBits = 1;
        fixtureDef.filter.maskBits = 1;
        body.createFixture(fixtureDef);
        polygonShape.dispose();
    }

    @Override
    public void update(float delta) {
        // Call the update method in the superclass (Scrollable)
        super.update(delta);
    }

    @Override
    public void reset(float newX) {
        super.reset(newX);
        this.body.setTransform(this.getX(), this.getY(), 0);
    }

    public void onRestart(float x, float scrollSpeed) {
        velocity.x = scrollSpeed;
        reset(x);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
