package GameWorld;

import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.infinityCrash.GameObjects.Ground;
import com.infinityCrash.GameObjects.Player;
import com.infinityCrash.GameObjects.ScrollHandler;
import com.infinityCrash.ICHelpers.AssetLoader;

import static com.infinityCrash.ICHelpers.Constants.*;

public class GameWorld {

	private Player player;
	private Ground groundObject, deadGround;
	
	private ScrollHandler scroller;
	private int score = 0;
	private GameRenderer renderer;
	
	private GameState currentState;
	
	
	// Box2D
	private World world;

	public enum GameState {
		MENU, READY, RUNNING, GAMEOVER, PAUSED
	}

	public GameWorld() {
		currentState = GameState.READY;

		// Box2D
		world = new World(WORLD_GRAVITY, false);
		
		scroller = new ScrollHandler(this, world, GROUND_RECTANGLE_Y);
		
		
		player = new Player(world, PLAYER_X, PLAYER_Y, PLAYER_WIDTH, PLAYER_HEIGHT);
		player.getBody().setUserData(player);
		player.setUserName(AssetLoader.prefs.getString("username"));

		groundObject = new Ground(world, GROUND_OBJECT_X, GROUND_OBJECT_Y, GROUND_OBJECT_WIDTH, GROUND_OBJECT_HEIGHT);

		deadGround = new Ground(world, GROUND_OBJECT_X, GAME_HEIGHT, GROUND_OBJECT_WIDTH, GROUND_OBJECT_HEIGHT);
		Filter f = new Filter();
		f.categoryBits = 2;
		f.maskBits = 2;
		deadGround.getBody().getFixtureList().first().setFilterData(f);
	}

	public void update(float delta) {

		switch (currentState) {
		case READY:
		case MENU:
			updateReady(delta);
			break;

		case RUNNING:
			updateRunning(delta);
			break;
		default:
			break;
		}

	}

	private void updateReady(float delta) {
		scroller.updateReady(delta);
	}

	public void updateRunning(float delta) {
		if (delta > .15f) {
			delta = .15f;
		}

		player.update(delta);
		scroller.update(delta);
	}

	public Player getPlayer() {
		return player;

	}

	public ScrollHandler getScroller() {
		return scroller;
	}

	public int getScore() {
		return score;
	}

	public void addScore(int increment) {
		score += increment;
	}

	public void start() {
		currentState = GameState.RUNNING;
		player.onRestart();
		scroller.onRestart();
	}

	public void ready() {
		currentState = GameState.READY;
		renderer.prepareTransition(0, 0, 0, 1f);
	}

	public void restart() {
		score = 0;
		player.onRestart();
		scroller.onRestart();
		ready();
	}

	public boolean isReady() {
		return currentState == GameState.READY;
	}

	public boolean isGameOver() {
		return currentState == GameState.GAMEOVER;
	}

	public boolean isRunning() {
		return currentState == GameState.RUNNING;
	}

	public boolean isPaused() {
		return currentState == GameState.PAUSED;
	}

	public void setRenderer(GameRenderer renderer) {
		this.renderer = renderer;
	}
	
	public World getWorld() {
		return this.world;
	}

	public Ground getGroundObject() {
		return this.groundObject;
	}

	public void setGameState(GameState gameState) {
		this.currentState = gameState;
	}

}
