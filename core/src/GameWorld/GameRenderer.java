package GameWorld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.infinityCrash.GameObjects.SimpleBox;
import com.infinityCrash.GameObjects.RewardBox;
import com.infinityCrash.GameObjects.Grass;
import com.infinityCrash.GameObjects.Hole;
import com.infinityCrash.GameObjects.Platform;
import com.infinityCrash.GameObjects.Player;
import com.infinityCrash.GameObjects.ScrollHandler;
import com.infinityCrash.ICHelpers.AssetLoader;
import com.infinityCrash.ICHelpers.InputHandler;
import com.infinityCrash.TweenAccessors.Value;
import com.infinityCrash.TweenAccessors.ValueAccessor;
import com.infinityCrash.ui.SimpleButton;

import java.util.List;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import static com.infinityCrash.ICHelpers.Constants.GAME_HEIGHT;
import static com.infinityCrash.ICHelpers.Constants.GAME_NAME;
import static com.infinityCrash.ICHelpers.Constants.GAME_WIDTH;
import static com.infinityCrash.ICHelpers.Constants.PLAYER_HEIGHT;

public class GameRenderer {

    private GameWorld myWorld;
    private OrthographicCamera cam;
    public Viewport viewport;
    private ShapeRenderer shapeRenderer;

    private SpriteBatch batcher;

    private int midPointY;

    // Game Objects
    private Player player;
    private ScrollHandler scroller;
    private Grass frontGrass, backGrass;
    private SimpleBox simpleBox1, simpleBox2, simpleBox3, simpleBox4, simpleBox5, simpleBox6;
    private SimpleBox[] simpleBoxes;
    private Hole hole;
    private Platform platform;
    private RewardBox rewardBox;

    // Game Assets
    private TextureRegion jumpUp, jumpDown;
    private Texture grass;
    private Texture ready;
    private Texture gameOver;
    private TextureRegion gameOverRetryButton, gameOverBackButton;

    private Animation holeAsset;

    private Texture nebula, backgroundCity, boxForLife, star1;
    private Texture[] scores;

    // Tween stuff
    private TweenManager manager;
    private Value alpha = new Value();
    private Color transitionColor;

    // BOX2D
    Sprite spritePlayer;

    private double positionBackground;
    private double positionBackgroundBack;

    private double positionNebula;
    private double positionNebulaBack;
    private double positionStar1X, positionStar1Y;

    private float scrollBackgroundVelocity;
    private float scrollNebulaVelocity, scrollStar1Velocity;


    public GameRenderer(final GameWorld world, int gameHeight, int midPointY) {
        myWorld = world;

        this.midPointY = midPointY;

        cam = new OrthographicCamera();
        cam.setToOrtho(true, GAME_WIDTH, gameHeight);
        viewport = new FitViewport(1920, 1080, cam);

        batcher = new SpriteBatch();
        batcher.setProjectionMatrix(cam.combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(cam.combined);

        initGameObjects();
        initAssets();
        createCollisionListener();

        transitionColor = new Color();
        prepareTransition(255, 255, 255, .5f);

        spritePlayer = new Sprite();

        positionBackground = 0;
        positionBackgroundBack = GAME_WIDTH;

        positionNebula = 0;
        positionNebulaBack = GAME_WIDTH;

        positionStar1X = 20;
        positionStar1Y = 10;

        scrollBackgroundVelocity = 0.0025f;
        scrollNebulaVelocity = 0.0050f;
        scrollStar1Velocity = 0.0075f;

    }

    private void initGameObjects() {
        player = myWorld.getPlayer();
        scroller = myWorld.getScroller();
        frontGrass = scroller.getFrontGrass();
        backGrass = scroller.getBackGrass();
        simpleBox1 = scroller.getSimpleBox1();
        simpleBox2 = scroller.getSimpleBox2();
        simpleBox3 = scroller.getSimpleBox3();
        simpleBox4 = scroller.getSimpleBox4();
        simpleBox5 = scroller.getSimpleBox5();
        simpleBox6 = scroller.getSimpleBox6();
        simpleBoxes = new SimpleBox[]{simpleBox1, simpleBox2, simpleBox3, simpleBox4, simpleBox5, simpleBox6};
        hole = scroller.getHole();
        platform = scroller.getPlatform();
        rewardBox = scroller.getRewardBox();
    }

    private void initAssets() {
        grass = AssetLoader.suelo;
        ready = AssetLoader.ready;
        gameOver = AssetLoader.gameOver;
        gameOverRetryButton = AssetLoader.gameOverRetry;
        gameOverBackButton = AssetLoader.gameOverBack;
        boxForLife = AssetLoader.box;
        backgroundCity = AssetLoader.gameBackground;
        scores = AssetLoader.scores;
        holeAsset = AssetLoader.holeSkewers;
        jumpUp = AssetLoader.jumpUp;
        jumpDown = AssetLoader.jumpDown;
        nebula = AssetLoader.nebula;
        star1 = AssetLoader.star1;
    }

    private void drawGrass() {
        // Draw the grass
        batcher.draw(grass, frontGrass.getX(), frontGrass.getY() + midPointY / 2,
                frontGrass.getWidth(), -midPointY / 2);

        batcher.draw(grass, backGrass.getX(), backGrass.getY() + midPointY / 2,
                backGrass.getWidth(), -midPointY / 2);
    }

    private void drawHole(float runTime) {
        hole.body.setTransform(hole.getX(), hole.getY(), 0);
        batcher.enableBlending();
        batcher.draw(backgroundCity, hole.getX() - 0.4f, hole.getY(), hole.getWidth() + 0.8f, 2);

        batcher.draw(holeAsset.getKeyFrame(runTime, true), hole.getX() - 0.4f, hole.getY(), hole.getWidth() + 0.8f, 2);
        batcher.disableBlending();
    }

    private void drawPlatform() {
        platform.body.setTransform(platform.getX(), platform.getY(), 0);
        batcher.draw(platform.textureRegionPlatform, platform.getX(), platform.getY(), platform.getWidth(), platform.getHeight());
        if (!rewardBox.ISGONNABEDESTROYED) {
            rewardBox.body.setTransform(rewardBox.getX(), rewardBox.getY(), 0);
            batcher.draw(rewardBox.textureRegionBox, rewardBox.getX(), rewardBox.getY(), rewardBox.getWidth(), rewardBox.getHeight());
        } else {
            rewardBox.stateExplosion += Gdx.graphics.getDeltaTime();
            rewardBox.body.setTransform(rewardBox.getX(), rewardBox.getY(), 0);
            batcher.draw(rewardBox.boxExplosion.getKeyFrame(rewardBox.stateExplosion, false), rewardBox.getX(), rewardBox.getY(), rewardBox.getWidth(),
                    rewardBox.getHeight());
            if (rewardBox.boxExplosion.isAnimationFinished(rewardBox.stateExplosion)) {
                rewardBox.destroyable = true;
                rewardBox.ISGONNABEDESTROYED = false;
                rewardBox.stateExplosion = 0;
            }
        }
    }

    private void drawBoxes() {
        for (SimpleBox simpleBox : simpleBoxes) {
            if (!simpleBox.ISGONNABEDESTROYED) {
                simpleBox.body.setTransform(simpleBox.getX(), simpleBox.getY(), 0);
                batcher.draw(simpleBox.textureRegionBox, simpleBox.getX(), simpleBox.getY(), simpleBox.getWidth(),
                        simpleBox.getHeight());
            } else {
                simpleBox.stateExplosion += Gdx.graphics.getDeltaTime();
                batcher.enableBlending();
                simpleBox.body.setTransform(simpleBox.getX(), simpleBox.getY(), 0);
                batcher.draw(simpleBox.boxExplosion.getKeyFrame(simpleBox.stateExplosion, false), simpleBox.getX(), simpleBox.getY(), simpleBox.getWidth(),
                        simpleBox.getHeight());
                batcher.disableBlending();
                if (simpleBox.boxExplosion.isAnimationFinished(simpleBox.stateExplosion)) {
                    simpleBox.destroyable = true;
                    simpleBox.ISGONNABEDESTROYED = false;
                    simpleBox.stateExplosion = 0;
                }
            }
        }
    }

    private void drawPlayer(float runTime) {

        if (player.getBody().getPosition().x < 0) {
            player.killPlayer();
        }

        if (player.getBody().getPosition().x < 1.5f) {
            player.getBody().setLinearVelocity(4f, 0f);
        } else if (player.getBody().getPosition().x > 2.5f) {
            player.getBody().setLinearVelocity(-2f, 0f);
        } else if (player.getBody().getLinearVelocity().x >= 0.5f) {
            player.getBody().setLinearVelocity(0f, 0f);
        }

        spritePlayer.setPosition(player.getBody().getPosition().x, player
                .getBody().getPosition().y);

        if (player.getBody().getLinearVelocity().y == 0) {
            spritePlayer.setRegion(player.getPlayerAnimation().getKeyFrame(runTime, true));
        } else if (player.getBody().getLinearVelocity().y < 0) {
            spritePlayer.setRegion(jumpUp);
        } else {
            spritePlayer.setRegion(jumpDown);
        }

        if (player.getDead()) {
            spritePlayer.setRegion(AssetLoader.dizzyAnimation.getKeyFrame(runTime, true));
            batcher.draw(spritePlayer, player.getBody().getPosition().x,
                    player.getBody().getPosition().y, player.getWidth(),
                    player.getHeight());
        } else {
            batcher.draw(spritePlayer, player.getBody().getPosition().x,
                    player.getBody().getPosition().y, player.getWidth(),
                    player.getHeight());
        }
    }

    private void drawReady() {
        batcher.draw(ready, GAME_WIDTH / 2 - 2, midPointY, 4, -1);
    }

    private void drawGameOver() {
        batcher.draw(gameOver, GAME_WIDTH / 2 - 1, midPointY - .5f, 2.3f, -.7f);
        batcher.draw(gameOverRetryButton, GAME_WIDTH / 2 - 2, midPointY + 2f, 1f, -1f);
        batcher.draw(gameOverBackButton, GAME_WIDTH / 2 + 1, midPointY + 2f, 1f, -1f);
    }

    private void drawScore() {
        int length = ("" + myWorld.getScore()).length();
        int score = myWorld.getScore();
        float x = GAME_WIDTH / 2;
        for (int i = 0; i < length; i++) {
            batcher.draw(scores[score % 10], x, 0.3f, 0.2f, -0.2f);
            score = score / 10;
            x = x - 0.25f;
        }
        float lifePos = .3f;
        for (int i = 0; i < player.getLife(); i++) {
            batcher.draw(boxForLife, lifePos, 0.3f, 0.2f, -0.2f);
            lifePos += .4f;
        }
    }

    public void render(float delta, float runTime) {
        if (!myWorld.isPaused()) {
            myWorld.getWorld().step(Gdx.graphics.getDeltaTime(), 6, 2);
        }

        Gdx.gl.glClearColor(34, 17, 64, 0);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        shapeRenderer.begin(ShapeType.Filled);

        // Draw Background color
        shapeRenderer.setColor(55 / 255.0f, 80 / 255.0f, 100 / 255.0f, 1);
        shapeRenderer.rect(0, 0, GAME_WIDTH, midPointY + 0.5f);

        // Draw Dirt
        shapeRenderer.setColor(183, 166, 166, 1);
        shapeRenderer.rect(0, midPointY + 0.5f, GAME_WIDTH, 4);

        shapeRenderer.end();

        batcher.begin();

        if (positionBackground <= -GAME_WIDTH) {
            positionBackground = positionBackgroundBack + GAME_WIDTH;
        }
        positionBackground -= scrollBackgroundVelocity;
        if (positionBackgroundBack <= -GAME_WIDTH) {
            positionBackgroundBack = positionBackground + GAME_WIDTH;
        }
        positionBackgroundBack -= scrollBackgroundVelocity;

        if (positionNebula <= -GAME_WIDTH) {
            positionNebula = positionNebulaBack + GAME_WIDTH;
        }
        positionNebula -= scrollNebulaVelocity;
        if (positionNebulaBack <= -GAME_WIDTH) {
            positionNebulaBack = positionNebula + GAME_WIDTH;
        }
        positionNebulaBack -= scrollNebulaVelocity;

        if (positionStar1X < -30) {
            positionStar1X = 20;
            positionStar1Y = 10;
        }
        positionStar1X -= scrollStar1Velocity;
        positionStar1Y -= scrollStar1Velocity;

        batcher.draw(backgroundCity, (float) positionBackground, 4, (GAME_WIDTH), -4);
        batcher.draw(backgroundCity, (float) positionBackgroundBack + (GAME_WIDTH), 4, -(GAME_WIDTH), -4);

        batcher.draw(nebula, (float) positionNebula, 4, (GAME_WIDTH), -4);
        batcher.draw(nebula, (float) positionNebulaBack + (GAME_WIDTH), 4, -(GAME_WIDTH), -4);

        batcher.draw(star1, (float) positionStar1X, (float) positionStar1Y, 15, 14.284124629f);

        batcher.disableBlending();
        drawBoxes();
        drawGrass();
        drawHole(runTime);
        batcher.enableBlending();
        drawPlatform();

        batcher.draw(AssetLoader.buttonDown, 9f, 4, 0.5f, 0.5f, 1f, 1f, 1, 1, 90f, 0, 0, AssetLoader.buttonDown.getWidth(), AssetLoader.buttonDown.getHeight(), false, false);
        batcher.draw(AssetLoader.buttonDown, 8f, 5, 1f, -1f);
        InputHandler.getPlayPauseButton().draw(batcher);


        if (myWorld.isRunning() || myWorld.isPaused()) {
            drawPlayer(runTime);
            drawScore();
        } else if (myWorld.isReady()) {
            drawPlayer(runTime);
            drawReady();
        } else if (myWorld.isGameOver()) {
            drawPlayer(runTime);
            drawGameOver();
        }
        batcher.end();
        drawTransition(delta);
    }

    public void prepareTransition(int r, int g, int b, float duration) {
        transitionColor.set(r / 255.0f, g / 255.0f, b / 255.0f, 1);
        alpha.setValue(1);
        Tween.registerAccessor(Value.class, new ValueAccessor());
        manager = new TweenManager();
        Tween.to(alpha, -1, duration).target(0)
                .ease(TweenEquations.easeOutQuad).start(manager);
    }

    private void drawTransition(float delta) {
        if (alpha.getValue() > 0) {
            manager.update(delta);
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            shapeRenderer.begin(ShapeType.Filled);
            shapeRenderer.setColor(transitionColor.r, transitionColor.g,
                    transitionColor.b, alpha.getValue());
            shapeRenderer.rect(0, 0, GAME_WIDTH, GAME_HEIGHT);
            shapeRenderer.end();
            Gdx.gl.glDisable(GL20.GL_BLEND);
        }
    }

    private void createCollisionListener() {
        myWorld.getWorld().setContactListener(
                new ContactListener() {
                    @Override
                    public void beginContact(Contact contact) {
                        if (((contact.getFixtureA().getBody() == myWorld.getGroundObject().getBody()
                                || (contact.getFixtureA().getBody().getUserData() != null && (contact.getFixtureA().getBody().getUserData() instanceof SimpleBox
                                || contact.getFixtureA().getBody().getUserData() instanceof RewardBox
                                || contact.getFixtureA().getBody().getUserData() instanceof Platform))) &&
                                contact.getFixtureB().getBody() == player.getBody()) || ((contact.getFixtureB().getBody() == myWorld.getGroundObject().getBody() || (contact.getFixtureB().getBody().getUserData() != null && (contact.getFixtureB().getBody().getUserData() instanceof SimpleBox
                                || contact.getFixtureB().getBody().getUserData() instanceof RewardBox
                                || contact.getFixtureB().getBody().getUserData() instanceof Platform))) &&
                                contact.getFixtureA().getBody() == player.getBody())) {
                            player.canJump = true;
                            if ((contact.getFixtureA().getBody() == myWorld.getGroundObject().getBody()) || (contact.getFixtureB().getBody() == myWorld.getGroundObject().getBody())) {
                                player.isLanded = true;
                                player.canJump = true;
                            }
                            if (contact.getFixtureA().getBody().getUserData() != null && contact.getFixtureA().getBody().getUserData() instanceof SimpleBox) {
                                if (((SimpleBox) contact.getFixtureA().getBody().getUserData()).getY() <= player.getY() + PLAYER_HEIGHT + 0.4 && ((SimpleBox) contact.getFixtureA().getBody().getUserData()).getY() >= player.getY() + PLAYER_HEIGHT - 0.4) {
                                    ((SimpleBox) contact.getFixtureA().getBody().getUserData()).ISGONNABEDESTROYED = true;
                                    ((SimpleBox) contact.getFixtureA().getBody().getUserData()).isScored = true;
                                    player.canJump = true;
                                }
                            }
                            if (contact.getFixtureA().getBody().getUserData() != null && contact.getFixtureA().getBody().getUserData() instanceof RewardBox) {
                                if (((RewardBox) contact.getFixtureA().getBody().getUserData()).getY() <= player.getY() + PLAYER_HEIGHT + 0.4 && ((RewardBox) contact.getFixtureA().getBody().getUserData()).getY() >= player.getY() + PLAYER_HEIGHT - 0.4) {
                                    ((RewardBox) contact.getFixtureA().getBody().getUserData()).ISGONNABEDESTROYED = true;
                                    ((RewardBox) contact.getFixtureA().getBody().getUserData()).isScored = true;
                                    player.canJump = true;
                                }
                            }
                        }
                    }

                    @Override
                    public void endContact(Contact contact) {
                        if (((contact.getFixtureA().getBody() == myWorld.getGroundObject().getBody() || (contact.getFixtureA().getBody().getUserData() != null && (contact.getFixtureA().getBody().getUserData() instanceof SimpleBox || contact.getFixtureA().getBody().getUserData() instanceof RewardBox
                                || contact.getFixtureA().getBody().getUserData() instanceof Platform))) &&
                                contact.getFixtureB().getBody() == player.getBody()) || ((contact.getFixtureB().getBody() == myWorld.getGroundObject().getBody() || (contact.getFixtureB().getBody().getUserData() != null && (contact.getFixtureB().getBody().getUserData() instanceof SimpleBox || contact.getFixtureB().getBody().getUserData() instanceof RewardBox
                                || contact.getFixtureB().getBody().getUserData() instanceof Platform))) &&
                                contact.getFixtureA().getBody() == player.getBody())) {
                            player.canJump = false;
                            if ((contact.getFixtureA().getBody() == myWorld.getGroundObject().getBody()) || (contact.getFixtureB().getBody() == myWorld.getGroundObject().getBody())) {
                                player.isLanded = false;
                            }
                        }
                    }

                    @Override
                    public void preSolve(Contact contact, Manifold oldManifold) {
                        if (contact.getFixtureA().getBody().getUserData() != null && contact.getFixtureA().getBody().getUserData() instanceof Hole) {
                            Filter f = new Filter();
                            f.categoryBits = 2;
                            f.maskBits = 2;
                            contact.getFixtureB().setFilterData(f);
                            player.killPlayer();
                        }
                    }

                    @Override
                    public void postSolve(Contact contact, ContactImpulse impulse) {
                    }
                }
        );
    }
}
